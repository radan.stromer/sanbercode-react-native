var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log(`${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}`);


var kalimat = "I am going to be React Native Developer"; 
var sentence = kalimat.split(" ");

var exampleFirstWord = sentence[0] ; 
var secondWord = sentence[1]; 
var thirdWord = sentence[2]; // lakukan sendiri 
var fourthWord = sentence[3]; // lakukan sendiri 
var fifthWord = sentence[4]; // lakukan sendiri 
var sixthWord = sentence[5]; // lakukan sendiri 
var seventhWord = sentence[6]; // lakukan sendiri 
var eighthWord = sentence[7]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord);


var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substr(4, 10); // do your own! 
var thirdWord2 = sentence2.substr(15, 2); // do your own! 
var fourthWord2 = sentence2.substr(18, 2); // do your own! 
var fifthWord2 = sentence2.substr(21, 24); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substr(4, 10); // do your own! 
var thirdWord3 = sentence3.substr(15, 2); // do your own! 
var fourthWord3 = sentence3.substr(18, 2); // do your own! 
var fifthWord3 = sentence3.substr(21, 24); // do your own! 

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;
// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' +  secondWord3 +', with length: ' + secondWordLength); 
console.log('Third Word: ' +  thirdWord3 +', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' +  fourthWord3 +', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' +  fifthWord3 +', with length: ' + fifthWordLength); 


// function range(startNum, finishNum){
//     if(!finishNum || !startNum ){
//         console.log('-1');
//     }else{
//         var numbers = [startNum];
//         if(finishNum < startNum){
//             while(finishNum < startNum) { 
//                 startNum--;
//                 numbers.push(startNum);
//             }
//         }else{
//             while(startNum < finishNum) { 
//                 startNum++;
//                 numbers.push(startNum);
//             }
//         }
//         console.log(numbers);
//     }
// }
// range(1,10);
// range(1);
// range(11,18);
// range(54,50);
// range();

function rangeWithStep(startNum, finishNum,step){
    if(!finishNum || !startNum ){
        console.log('-1');
    }else{
        var numbers = [startNum];
        if(finishNum < startNum){
            while(finishNum < startNum) { 
                startNum = startNum - step;
                if(startNum>=finishNum)numbers.push(startNum);
            }
        }else{
            while(startNum < finishNum) { 
                startNum = startNum + step;
                if(startNum<=finishNum)numbers.push(startNum);
            }
        }
        console.log(numbers);
    }
}
    
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 